package etsy

import (
  "net/url"
  "path"
  "strconv"
  "time"
)

// EtysUrl structure of Etsy.com url. Making default EtsyURL.URL to https://openapi.etsy.com/v2
// For path adding use function AddPath
// For parameters Add or Set, functions: SetParam(), SetParamI(), AddParam(), AddParamI()
// Generate url string with function String(), whish return examples:
// http://openapi.etsy.com/v2/shops/shop_id/receipts;
// http://openapi.etsy.com/v2/countries
type EtsyUrl struct {
  URL     *url.URL
  Query   *url.Values
}

// NewUrl create Etsy.com default url and return EtsyUrl
func NewUrl() *EtsyUrl {
  return &EtsyUrl{
    URL: &url.URL{
      Scheme: "https",
      Host: "openapi.etsy.com",
      Path: "v2",
    },
    Query: &url.Values{},
  }
}

// AddPath adding to EtsyUrl Url path parameters
func (u *EtsyUrl) AddPath(args ...string) *EtsyUrl {
  arg := path.Join(args...)
  u.URL.Path = path.Join(u.URL.Path, arg)
  return u
}

// String encode Url parameters.
// Example: ?param=1&param=2...
func (u *EtsyUrl) String() string {
  u.URL.RawQuery = u.Query.Encode()
  return u.URL.String()
}

func (u *EtsyUrl) Set(key string, value interface{}) *EtsyUrl {
  switch v := value.(type) {
  case int:
    u.Query.Set(key, strconv.Itoa(v))
    break
  case string:
    u.Query.Set(key, v)
    break
  case bool:
    u.Query.Set(key, strconv.FormatBool(v))
    break
  case *bool:
    u.Query.Set(key, strconv.FormatBool(*v))
    break
  case time.Time:
    u.Query.Set(key, time2string(&v))
    break
  case *time.Time:
    u.Query.Set(key, time2string(v))
    break
  }

  return u
}

// SetParam set string parameter for url query.
// If parameter exist function replaced to new value
func (u *EtsyUrl) SetParam(key, value string) *EtsyUrl {
  u.Query.Set(key, value)
  return u
}


// SetParam set integer parameter for url query
// If parameter exist function replaced to new value
func (u *EtsyUrl) SetParamI(key string, value int) *EtsyUrl {
  u.SetParam(key, strconv.Itoa(value))
  return u
}

// AddParam adding string parameter for url query
func (u *EtsyUrl) AddParam(key, value string) *EtsyUrl {
  u.Query.Add(key, value)
  return u
}

// AddParam adding integer parameter for url query
func (u *EtsyUrl) AddParamI(key string, value int) *EtsyUrl {
  u.AddParam(key, strconv.Itoa(value))
  return u
}

func time2string(t *time.Time) string {
  return strconv.FormatInt(t.Unix(), 10)
}
