package etsy

import (
  "testing"
  "os"
)

var (
  ETSY_KEYSTRING    = os.Getenv("ETSY_KEYSTRING")
  ETSY_SHAREDSECRET = os.Getenv("ETSY_SHAREDSECRET")
  ETSY_AUTHTOKEN    = os.Getenv("ETSY_AUTHTOKEN")
  ETSY_AUTHSECRET   = os.Getenv("ETSY_AUTHSECRET")
)

func TestAuthorize(t *testing.T) {
  client := New(ETSY_KEYSTRING, ETSY_SHAREDSECRET)
  err := client.Authorize(ETSY_AUTHTOKEN, ETSY_AUTHSECRET)
  if err != nil {
    t.Error(err)
  }
}
