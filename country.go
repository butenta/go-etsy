package etsy

import (
  "strconv"
  "encoding/json"
)

// Country - Represents a geographical country and its location.
type Country struct {
  // The country's numeric ID.
  Id        int       `json:"country_id"`

  // IsoCode - The two-letter country code according to ISO 3166-1-alpha-2.
  IsoCode   string    `json:"iso_country_code"`

  // BankCode - The three-letter country code according to the World Bank.
  BankCode  string    `json:"world_bank_country_code"`

  // Name - The country's plain-English name.
  Name      string    `json:"name"`

  // Slug - The country's plain-English name slugified; suitable for
  // interpolation into a url.
  Slug      string    `json:"slug"`

  // The country's latitude.
  Lat       float64   `json:"lat,omitempty"`

  // Lon - The country's longitude.
  Lon       float64   `json:"lon,omitempty"`
}

// Countries - Represents list of countries
type Countries struct {
  // Count - How much countries exist in Results
  Count   int         `json:"count"`

  // Results - Slice of Country
  Results []Country   `json:"results"`
}

// NewCountries create and return empty pointer to Countries
func NewCountries() *Countries {
  return &Countries{}
}

// Decode data from XML to Countries structure, body - byte data from url
func (c *Countries) Decode(body []byte) (*Countries, error) {
  err := json.Unmarshal(body, c)
  return c, err
}

// GetCountries return all countries with id nil, or get coyntry by id
func (e *Etsy) GetCountries(id int, iso string) (*Countries, error) {
  url := NewUrl().AddPath("countries")
  if id != 0 {
    url = url.AddPath(strconv.Itoa(id))
  } else if iso != "" {
    url = url.AddPath("iso", iso)
  }

  body, err := e.Get(url)
  if err != nil {
    return nil, err
  }

  return NewCountries().Decode(body)
}
