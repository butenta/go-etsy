package etsy

import (
  // "strconv"
  "encoding/json"
  "fmt"
)

type Transaction struct {
  Id                int       `json:"transaction_id"`
  Title             string
  Description       string
  SellerId          int       `json:"seller_user_id"`
  BuyerId           int       `json:"buer_user_id"`
  CreationTsz       int       `json:"creation_tsz"`
  PaidTsz           int       `json:"paid_tsz"`
  ShippedTsz        int       `json:"shipped_tsz"`
  Price             float64   `json:"price,string"`
  Currency          string    `json:"currency_code"`
  Quantity          int       `json:"quantity"`
  Tags              []string  `json:"tags"`
  Materials         []string  `json:"materials"`
  ImgListId         int       `json:"image_list_id"`
  ReceiptId         int       `json:"receipt_id"`
  ShippingCost      float64   `json:"shipping_cost,string"`
  IsDigital         bool      `json:"is_digital"`
  FileData          string    `json:"file_data"`
  ListingId         int       `json:"listing_id"`
  IsQuickSale       bool      `json:"is_quick_sale"`
  SellerFeedBackId  int       `json:"seller_feedback_id"`
  BuyerFeedBackId   int       `json:"buyer_feedback_id"`
  TransactionType   string    `json:"transaction_type"`
  Url               string    `json:"url"`
  ProductData                 `json:"product_data"`
}

type Transactions struct {
  Count   int           `json:"count"`
  Results []Transaction `json:"results"`
}

func NewTransactions() *Transactions {
  return &Transactions{}
}

func (t *Transactions) Decode(body []byte) (*Transactions, error) {
  err := json.Unmarshal(body, t)
  return t, err
}

// GetCountries return all countries with id nil, or get coyntry by id
func (e *Etsy) GetTransactions(shopId string) (*Transactions, error) {
  url := NewUrl().AddPath("receipts", shopId, "transactions")
  body, err := e.Get(url)
  if err != nil {
    return nil, err
  }
  fmt.Println(string(body))

  return NewTransactions().Decode(body)
}

type ProductData struct {
  ProductId int     `json:"product_id"`
  Sku       string  `json:"sku"`
}
