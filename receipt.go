package etsy

import (
  "encoding/json"
)

type ReceiptShipment struct {
  CarrierName         string          `json:"carrier_name"`
  ReceiptShippingId   int             `json:"receipt_shipping_id"`
  TrackingCode        string          `json:"tracking_code"`
  TrackingUrl         string          `json:"tracking_url"`
  BuyerNote           string          `json:"buyer_note"`
  NotificationDate    int             `json:"notification_date"`
}

type Receipt struct {
  ReceiptId       int                 `json:"receipt_id"`
  ReceiptType     int                 `json:"receipt_type"`
  OrderId         int                 `json:"order_id"`
  SellerUserId    int                 `json:"seller_user_id"`
  BuyerUserId     int                 `json:"buyer_user_id"`
  CreationTsz     int64               `json:"creation_tsz"`
  CanRefund       bool                `json:"can_refund"`
  LastModifiedTsz int64               `json:"last_modified_tsz"`
  Name            string              `json:"name"`
  FirstLine       string              `json:"first_line"`
  SecondLine      string              `json:"second_line"`
  City            string              `json:"city"`
  State           string              `json:"state"`
  Zip             string              `json:"zip"`
  FormattedAddr   string              `json:"formatted_address"`
  CountryId       int                 `json:"country_id"`
  PaymentMethod   string              `json:"payment_method"`
  PaymentEmail    string              `json:"payment_email"`
  SellerMessage   string              `json:"message_from_seller"`
  BuyerMessage    string              `json:"message_from_buyer"`
  WasPaid         bool                `json:"was_paid"`
  TotalTaxCost    float64             `json:"total_tax_cost,string"`
  TotalVatCost    float64             `json:"total_vat_cost,string"`
  TotalPrice      float64             `json:"total_price,string"`
  TotalShippCost  float64             `json:"total_shipping_cost,string"`
  CurrencyCode    string              `json:"currency_code"`
  PaymentMessage  string              `json:"message_from_payment"`
  WasShipped      bool                `json:"was_shipped"`
  BuyerEmail      string              `json:"buyer_email"`
  SellerEmail     string              `json:"seller_email"`
  IsGift          bool                `json:"is_gift"`
  GiftWrap        bool                `json:"needs_gift_wrap"`
  GiftMessage     string              `json:"gift_message"`
  GiftWrapPrice   float64             `json:"gift_wrap_price,string"`
  DiscountAmt     float64             `json:"discount_amt,string"`
  SubTotal        float64             `json:"subtotal,string"`
  GrandTotal      float64             `json:"grandtotal,string"`
  AdjGrandTotal   float64             `json:"adjusted_grandtotal,string"`
  Shipments       []ReceiptShipment
  Transactions    []Transaction       `json:"transactions"`
  Country         Country             `json:"country"`
}

// GetAllReceipts - getting all receipts from Etsy by shopId
func GetAllReceipts(client *Etsy, shopId string, url *EtsyUrl) ([]Receipt, error) {
  data, err := client.GetAll(url.AddPath("shops", shopId, "receipts"))
  if err != nil {
    return nil, err
  }

  receipts := []Receipt{}
  err = json.Unmarshal(data, &receipts)

  return receipts, err
}
