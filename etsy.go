package etsy

import (
  "net/http"
  "io/ioutil"
  "errors"
  "encoding/json"

  "github.com/mrjones/oauth"
)

var Endpoint = oauth.ServiceProvider{
	RequestTokenUrl:   NewUrl().AddPath("oauth/request_token").String(),
  AuthorizeTokenUrl: "https://www.etsy.com/oauth/signin",
	AccessTokenUrl:    NewUrl().AddPath("oauth/access_token").String(),
}

type Etsy struct {
  KeyString     string
  SharedSecret  string
  AccessToke    string
  AccessSecret  string
  Consumer      *oauth.Consumer
  Client        *http.Client
}

// New initialize Consumer and Etsy, return Etsy
func New(key, secret string) *Etsy {
  consumer := oauth.NewConsumer(key, secret, Endpoint)

  return &Etsy{
    KeyString: key,
    SharedSecret: secret,
    Consumer: consumer,
  }
}

// Debug enable or disable debuger for Etsy
func (e *Etsy) Debug(on bool) *Etsy {
  e.Consumer.Debug(on)
  return e
}

// Authorize OAuth authorization for get private Etsy data
func (e *Etsy) Authorize(token, secret string) (err error) {
  e.Client, err = e.Consumer.MakeHttpClient(
    &oauth.AccessToken{
      Token: token,
      Secret: secret,
  })

  return
}

func (e *Etsy) GetAll(url *EtsyUrl) (bytes []byte, err error) {
  var resp *Response
  var res []interface{}

  for {
    resp, err = e.GetResponse(url)
    if err != nil {
      return
    }

    res = append(res, resp.Results...)

    offset := resp.Pagination.NextOffset
    if offset == 0 {
      break
    }

    url.Set("offset", offset)
  }

  return json.Marshal(res)
}

// GetResponse call to function Get and get data in bytes
// and converting to Response value
func (e *Etsy) GetResponse(url *EtsyUrl) (*Response, error) {
  data, err := e.Get(url)
  if err != nil {
    return nil, err
  }
  // fmt.Printf("%v", string(data))

  response := &Response{}
  if err := response.UnmarshalJson(data); err != nil {
    return nil, err
  }

  return response, nil
}

// Get request Etsy API data by url and return bytes value
func (e *Etsy) Get(url *EtsyUrl) ([]byte, error) {
  response, err := e.Client.Get(url.String())
  if err != nil {
    return nil, err
  }
  defer response.Body.Close()

  body, err := ioutil.ReadAll(response.Body)
  if err != nil {
    return nil, err
  }

  if response.StatusCode != 200 {
    return nil, errors.New(string(body))
  }

  return body, nil
}

func (e *Etsy) GetRequestTokenAndUrl(callback string) (*oauth.RequestToken, string, error) {
  return e.Consumer.GetRequestTokenAndUrl(callback)
}

func (e *Etsy) AuthorizeToken(rtoken *oauth.RequestToken, verificationCode string) (*oauth.AccessToken, error) {
  return e.Consumer.AuthorizeToken(rtoken, verificationCode)
}

type Pagination struct {
  EffectiveLimit    int `json:"effective_limit"`
  EffectiveOffset   int `json:"effective_offset"`
  NextOffset        int `json:"next_offset"`
  EffectivePage     int `json:"effective_page"`
  NextPage          int `json:"next_page"`
}

type Response struct {
  Count       int             `json:"count"`
  Results     []interface{}   `json:"results"`
  Type        string          `json:"type"`
  Pagination  Pagination      `json:"pagination"`
}

func (r *Response) UnmarshalJson(data []byte) error {
  return json.Unmarshal(data, r)
}
